package si.pernat.primoz.android_reveel.helper;

import org.junit.Test;

import si.pernat.primoz.androidReveel.helper.Movie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Primoz on 25.1.16.
 */
public class MovieTest {

    @Test
    public void addition_isCorrect() throws Exception {
        Movie movie = new Movie();
        movie.setReleaseDate("2015-1-12");
        movie.setRevenue(1000);
        assertEquals("Same Date", "Jan 12, 2015", movie.getReleaseDate());
        assertEquals("Same revenue", "$1,000", movie.getRevenue());
        assertNotEquals("Not the same revenue return","$ 1000",movie.getRevenue());
    }
}
