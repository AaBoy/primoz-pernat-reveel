package si.pernat.primoz.androidReveel.helper;

/**
 * Created by Primoz on 23.1.16.
 */
public class Constants {
    private static final String API_KEY="ea0553ee0e776913b0b74647ae3ef46b";
    public static final String KEY="?api_key="+API_KEY;
    public static final String IMDB_KEY="?external_source=imdb_id&api_key="+API_KEY;
    public static final String CAST_KEY="/credits?api_key="+API_KEY;
    public static final String RATING_KEY="/rating?api_key="+API_KEY;
    public static final String VIDEO_KEY="/videos?api_key="+API_KEY;


    public static final String URL="https://api.themoviedb.org/3/movie/";
    public static final String URL_CAST="http://api.themoviedb.org/3/movie/";
    public static final String URL_IMDB="https://api.themoviedb.org/3/find/";
    public static final String URL_IMAGE="http://image.tmdb.org/t/p/w500/";
    public static final String URL_CAST_IMAGE="http://image.tmdb.org/t/p/w300/";
    public static final String URL_POST_RATING="http://api.themoviedb.org/3/movie/";
    public static final String URL_VIDEO="http://api.themoviedb.org/3/movie/";
    public static final String URL_GUEST_SESSION="http://api.themoviedb.org/3/authentication/guest_session/new"+KEY;

}
