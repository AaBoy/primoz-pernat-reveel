package si.pernat.primoz.androidReveel.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import si.pernat.primoz.androidReveel.R;
import si.pernat.primoz.androidReveel.fragment.MainFragment;

public class MainActivity extends AppCompatActivity implements MainFragment.FragmentListener {

    TextView toolbarTitle;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ico_menu);
        toolbarTitle = (TextView) findViewById(R.id.textview_toolbar);
        getFragmentManager().beginTransaction().replace(R.id.container, new MainFragment(), MainFragment.MAIN_FRAGMENT).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Log.d("TOOLBAR", "Search");
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolbarTitle(String title) {
        toolbarTitle.setText(title);
    }
}
