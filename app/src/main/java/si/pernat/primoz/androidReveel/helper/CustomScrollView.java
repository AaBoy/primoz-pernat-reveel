package si.pernat.primoz.androidReveel.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

/**
 * Created by Primoz on 24.1.16.
 */
public class CustomScrollView extends ScrollView {

    private boolean start;
    private int offset;

    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public interface OnScrollViewListener {
        void onScrollChanged(CustomScrollView v, int l, int t, int oldl, int oldt);

        void onBottomReached();
    }

    private OnScrollViewListener mOnScrollViewListener;

    public void setOnScrollViewListener(OnScrollViewListener l) {
        this.mOnScrollViewListener = l;
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        mOnScrollViewListener.onScrollChanged(this, l, t, oldl, oldt);


        View view = getChildAt(getChildCount() - 1);
        int diff = (view.getBottom() - (getHeight() + getScrollY()));

        if (!start) {
            offset = diff;
            start = true;
        }

        if (Math.abs(diff - offset) <= 10 && mOnScrollViewListener != null) {
            mOnScrollViewListener.onBottomReached();
        }
//        Log.d("Offset", diff + "");
        super.onScrollChanged(l, t, oldl, oldt);
    }
}
