package si.pernat.primoz.androidReveel.helper;

/**
 * Created by Primoz on 23.1.16.
 */
public class Cast {
    private String name;
    private String character;
    private String photoUrl;

    public Cast(String name, String character, String photoUrl) {
        this.name = name;
        this.character = character;
        this.photoUrl = photoUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
