package si.pernat.primoz.androidReveel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import si.pernat.primoz.androidReveel.R;
import si.pernat.primoz.androidReveel.helper.Genre;

/**
 * Created by Primoz on 23.1.16.
 */
public class GenreRecyclerViewAdapter extends RecyclerView.Adapter<GenreRecyclerViewAdapter.GenreHolder> {

    private ArrayList<Genre> genres;
    private LayoutInflater layoutInflater;

    public GenreRecyclerViewAdapter(Context context, ArrayList<Genre> genres) {
        layoutInflater = LayoutInflater.from(context);
        this.genres = genres;
    }

    @Override
    public GenreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_genre, parent, false);
        GenreHolder genreHolder = new GenreHolder(view);
        return genreHolder;
    }

    @Override
    public void onBindViewHolder(GenreHolder holder, int position) {
        holder.button.setText(genres.get(position).getGenre());
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    class GenreHolder extends RecyclerView.ViewHolder {
        Button button;

        public GenreHolder(View itemView) {
            super(itemView);
            button = (Button) itemView.findViewById(R.id.button_genre);
        }
    }
}
