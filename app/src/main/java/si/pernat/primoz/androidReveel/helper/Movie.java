package si.pernat.primoz.androidReveel.helper;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Primoz on 23.1.16.
 */
public class Movie {

    private double revenue;
    private String title;
    private double voteAverage;
    private int voteCount;
    private String releaseDate;
    private String runTime;
    private ArrayList<Genre> genres;
    private String overview;
    private double rating;
    private String storyline;
    private String imageUrl;
    private String videoUrl;
    private String id;


    public Movie(double revenue, String originalTitle, double voteAverage, int voteCount, String releaseDate, String runTime, ArrayList<Genre> genres, String overview, double rating) {
        this.revenue = revenue;
        this.title = originalTitle;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.releaseDate = releaseDate;
        this.runTime = runTime;
        this.genres = genres;
        this.overview = overview;
        this.rating = rating;
    }

    public Movie() {

    }

    public Movie(double revenue, String title, double voteAverage, int voteCount, String releaseDate, String runTime, ArrayList<Genre> genres, String overview, double rating, String storyline) {
        this.revenue = revenue;
        this.title = title;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.releaseDate = releaseDate;
        this.runTime = runTime;
        this.genres = genres;
        this.overview = overview;
        this.rating = rating;
        this.storyline = storyline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoryline() {
        return storyline;
    }

    public void setStoryline(String storyline) {
        this.storyline = storyline;
    }

    public String getRevenue() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(0);
        return formatter.format(revenue);
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public String getReleaseDate() {
//        Calendar mydate = new GregorianCalendar();
        Date thedate = null;
        try {
            thedate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(releaseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        return df.format(thedate);
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRunTime() {

        int time = Integer.parseInt(runTime);
        String tmp = time / 60 + " hr ";
        if (time % 60 == 0) {
            return tmp;
        }
        tmp += time % 60 + " min";

        return tmp;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
