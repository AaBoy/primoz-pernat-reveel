package si.pernat.primoz.androidReveel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import si.pernat.primoz.androidReveel.R;
import si.pernat.primoz.androidReveel.helper.Cast;
import si.pernat.primoz.androidReveel.helper.Constants;

/**
 * Created by Primoz on 23.1.16.
 */
public class CastRecyclerViewAdapter extends RecyclerView.Adapter<CastRecyclerViewAdapter.CastHolder> {


    private ArrayList<Cast> cast;
    private LayoutInflater inflater;
    private Context mContext;

    public CastRecyclerViewAdapter(Context context, ArrayList<Cast> cast) {
        this.cast = cast;
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public CastHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_cast, parent, false);
        CastHolder castHolder = new CastHolder(view);
        return castHolder;
    }

    @Override
    public void onBindViewHolder(CastHolder holder, int position) {
        holder.character.setText("- " + cast.get(position).getCharacter());
        holder.name.setText(cast.get(position).getName());
        Glide.with(mContext).load(Constants.URL_CAST_IMAGE + cast.get(position).getPhotoUrl()).into(holder.profilePhoto);
    }

    @Override
    public int getItemCount() {
        return cast.size();
    }

    class CastHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView character;
        ImageView profilePhoto;

        public CastHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.textview_name);
            character = (TextView) view.findViewById(R.id.textview_character);
            profilePhoto = (ImageView) view.findViewById(R.id.imgv_cast);
        }
    }
}
