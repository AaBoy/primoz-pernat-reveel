package si.pernat.primoz.androidReveel.helper;

/**
 * Created by Primoz on 23.1.16.
 */
public class JsonTAG {
    //TheMovieDatabase
    public static final String REVENUE = "revenue";
    public static final String ORIGINAL_TITLE = "original_title";
    public static final String VOTE_AVERAGE = "vote_average";
    public static final String VOTE_COUNT = "vote_count";
    public static final String RELEASE_DATE = "release_date";
    public static final String RUN_TIME = "runtime";
    public static final String GENRES = "genres";
    public static final String GENRE_NAME = "name";
    public static final String ID = "id";
    public static final String OVERVIEW = "overview";
    public static final String RATING = "rating";
    public static final String IMDB_ID = "imdb_id";
    public static final String MOVIE_RESULTS = "movie_results";
    public static final String STORYLINE = "overview";
    public static final String IMAGE_URL = "poster_path";
    public static final String VIDEO_URL = "";
    public static final String NAME = "name";
    public static final String IMAGE_PATH = "profile_path";
    public static final String CHARACTER = "character";
    public static final String RESULTS = "results";
    public static final String KEY = "key";
    public static final String GUEST_SESSION_ID="guest_session_id";
}
