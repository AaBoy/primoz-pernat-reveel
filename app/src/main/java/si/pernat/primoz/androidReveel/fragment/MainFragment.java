package si.pernat.primoz.androidReveel.fragment;


import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.commit451.nativestackblur.NativeStackBlur;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import si.pernat.primoz.androidReveel.R;
import si.pernat.primoz.androidReveel.adapter.CastRecyclerViewAdapter;
import si.pernat.primoz.androidReveel.adapter.GenreRecyclerViewAdapter;
import si.pernat.primoz.androidReveel.helper.Cast;
import si.pernat.primoz.androidReveel.helper.Constants;
import si.pernat.primoz.androidReveel.helper.CustomScrollView;
import si.pernat.primoz.androidReveel.helper.Genre;
import si.pernat.primoz.androidReveel.helper.JsonTAG;
import si.pernat.primoz.androidReveel.helper.Movie;
import si.pernat.primoz.androidReveel.volley.VolleySingleton;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {

    public static final String MAIN_FRAGMENT="MAIN_FRAGMENT";
    private RequestQueue requestQueue;
    private Movie movie;
    private ArrayList<Cast> cast;
    private ImageView imageView;
    private RecyclerView castRecyclerView;
    private RecyclerView genreRecyclerView;
    private GenreRecyclerViewAdapter genreAdapter;
    private CastRecyclerViewAdapter castAdapter;
    private boolean ratingFlag;
    private CustomScrollView scrollView;
    private ImageButton playButton;
    private boolean blureFlag;
    private Bitmap bitmap;
    private RatingBar ratingBar;
    private TextView title, rating, releaseDate, runTime, revenue, votes, avrgVote,
            overview, storyline, toolbarTitle;
    private ImageView circle;
    private FragmentListener fragmentListener;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        blureFlag = true;
        ratingFlag=true;
        setRetainInstance(true);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_main, container, false);
        requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

        toolbarTitle = (TextView) view.findViewById(R.id.textview_toolbar);

        fragmentListener= (FragmentListener) getActivity();

        circle= (ImageView) view.findViewById(R.id.imgv_circle);
        imageView = (ImageView) view.findViewById(R.id.imgv_film);
        castRecyclerView = (RecyclerView) view.findViewById(R.id.recyclreview_cast);
        castRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        genreRecyclerView = (RecyclerView) view.findViewById(R.id.recyclreview_genre);
        genreRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        ratingBar = (RatingBar) view.findViewById(R.id.rating);
        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(!ratingFlag){
                    Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.alredy_rated), Snackbar.LENGTH_LONG).show();
                    return true;
                }

                if (event.getAction() == MotionEvent.ACTION_UP && ratingFlag) {
                    float touchPositionX = event.getX();
                    float width = ratingBar.getWidth();
                    float starsf = (touchPositionX / width) * 10.0f;
                    int stars = (int) starsf + 1;
                    ratingBar.setRating(stars);
                    v.setPressed(false);
                    ratingFlag = false;
                    submitRating(stars);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setPressed(true);
                }

                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    v.setPressed(false);
                }
                return true;
            }
        });

        playButton = (ImageButton) view.findViewById(R.id.button_play);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getVideoUrl()));
                startActivity(browserIntent);
            }
        });

        scaleDown = ObjectAnimator.ofPropertyValuesHolder(circle,
                PropertyValuesHolder.ofFloat("scaleX", 2.5f),
                PropertyValuesHolder.ofFloat("scaleY", 2.5f));
        scaleDown.setDuration(1500);
        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.setStartDelay(100);
        scaleDown.start();

        ObjectAnimator anim = ObjectAnimator.ofFloat(circle, "alpha", 1f, 0.1f);
        anim.setDuration(1500);
        anim.setRepeatCount(ObjectAnimator.INFINITE);
        anim.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.setStartDelay(100);
        anim.start();


        scrollView = (CustomScrollView) view.findViewById(R.id.scroll_view);
        scrollView.setOnScrollViewListener(new CustomScrollView.OnScrollViewListener() {
            @Override
            public void onScrollChanged(CustomScrollView v, int l, int t, int oldl, int oldt) {

                if (bitmap == null)
                    return;

                if (oldt > 10) {
                    if (oldt < 410 && Math.abs(oldt - scroller) > 10) {
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight() - (oldt / 410) * 100, false);
                        playButton.setScaleType(ImageView.ScaleType.MATRIX);
                        imageView.setImageBitmap(NativeStackBlur.process(resizedBitmap, oldt / 2));
                        playButton.setVisibility(View.VISIBLE);
                        circle.setVisibility(View.VISIBLE);
                        float alpha = 1f - (((float) oldt / 410.0f) * 0.8f);
                        imageView.setAlpha(alpha);
                        scroller = oldt;
                        blureFlag = true;
//                        Log.d("BLURE", "nastavi vse");
                    } else if (oldt > 410) {
                        imageView.setAlpha(0.2f);
                    }

                    if (oldt > 200) {
                        playButton.setVisibility(View.INVISIBLE);
                        circle.setVisibility(View.INVISIBLE);
//                        Log.d("BLURE", "skrij cirlce");
                    }
                } else {
                    blureFlag = false;
                    imageView.setImageBitmap((bitmap));
                    playButton.setVisibility(View.VISIBLE);
                    circle.setVisibility(View.VISIBLE);
                    imageView.setAlpha(1f);
//                    Log.d("BLURE", "ponastavi vse");
                }

            }

            @Override
            public void onBottomReached() {
                if (blureFlag) {
                    imageView.setImageBitmap((bitmap));
                    playButton.setVisibility(View.VISIBLE);
                    circle.setVisibility(View.VISIBLE);
                    imageView.setAlpha(1f);
                    blureFlag = false;
                    Log.d("BLURE", "sem na koncu");
                }
            }

        });

        if(movie!=null){
            fillTextViews();
        }else {
            startRequest();
            setUpTextViews(view);
        }

        return view;
    }



    ObjectAnimator scaleDown;
    int scroller=0;
    private void submitRating(final int stars) {

        final String guestSessionId="";
        JsonObjectRequest requestGuestId = new JsonObjectRequest(
                Request.Method.GET,
                Constants.URL_GUEST_SESSION,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("ID",response.getString(JsonTAG.GUEST_SESSION_ID));
                          sendRating(stars, response.getString(JsonTAG.GUEST_SESSION_ID));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        requestQueue.add(requestGuestId);
    }


    private void sendRating(int stars,String sessionId){
        String url = Constants.URL_POST_RATING + movie.getId() + Constants.RATING_KEY+"&guest_session_id="+sessionId;

        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                new JSONObject(setParameters(stars)),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.rating_succed), Snackbar.LENGTH_LONG).show();
                        ratingFlag = false;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.rating_failed), Snackbar.LENGTH_LONG).show();
                        ratingFlag = true;
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };



        requestQueue.add(myRequest);
    }


    private Map<String, String> setParameters(int rating) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("value", "" + rating);
        return params;
    }




    private void startRequest() {
        movie = new Movie();
        Log.d("MOVIE_URL",Constants.URL + "264660" + Constants.KEY);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constants.URL + "264660" + Constants.KEY,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Log.d("JSON_STRING", response.toString());
                        try {
//                            Log.d("OPIS_DB", response.getString(JsonTAG.OVERVIEW).toString());
                            //Log.d("RATING",response.getString(JsonTAG.VOTE_AVERAGE));
                            movie.setReleaseDate(response.getString(JsonTAG.RELEASE_DATE));
                            movie.setVoteAverage(response.getDouble(JsonTAG.VOTE_AVERAGE));
                            movie.setVoteCount(response.getInt(JsonTAG.VOTE_COUNT));
                            movie.setTitle(response.getString(JsonTAG.ORIGINAL_TITLE));
                            movie.setStoryline(response.getString(JsonTAG.STORYLINE));
                            movie.setRunTime(response.getString(JsonTAG.RUN_TIME));
                            movie.setImageUrl(response.getString(JsonTAG.IMAGE_URL));
                            movie.setRevenue(response.getDouble(JsonTAG.REVENUE));
                            movie.setId(response.getInt(JsonTAG.ID) + "");
                            JSONArray jsonArray = response.getJSONArray(JsonTAG.GENRES);
                            movie.setGenres(new ArrayList<Genre>());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                movie.getGenres().add(new Genre(jsonArray.getJSONObject(i).getString(JsonTAG.GENRE_NAME)));
                            }
                            genreAdapter = new GenreRecyclerViewAdapter(getActivity(), movie.getGenres());
                            genreRecyclerView.setAdapter(genreAdapter);

                            Log.d("IMGAE_URL", Constants.URL_IMAGE + movie.getImageUrl());
                            Glide.with(getActivity()).load(Constants.URL_IMAGE + movie.getImageUrl()).asBitmap().into(new BitmapImageViewTarget(imageView) {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    super.onResourceReady(resource, glideAnimation);

                                    bitmap = resource;
                                }
                            });

                            getIMDB(response.get(JsonTAG.IMDB_ID).toString());
                            getVideoUrl();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("JSON_ERROR", error.toString());
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    private void getIMDB(final String imdId) {
        Log.d("IMDB_URL", Constants.URL_IMDB + imdId + Constants.IMDB_KEY);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constants.URL_IMDB + imdId + Constants.IMDB_KEY,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = response.getJSONArray(JsonTAG.MOVIE_RESULTS);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            movie.setOverview(jsonObject.getString(JsonTAG.OVERVIEW));
                            fetchCast(jsonObject.getInt(JsonTAG.ID) + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("JSON_ERROR", error.toString());
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    private void fetchCast(String imdId) {
        cast = new ArrayList<>();
        Log.d("IMDB_URL", Constants.URL_CAST + imdId + Constants.CAST_KEY);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constants.URL_CAST + imdId + Constants.CAST_KEY,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = response.getJSONArray("cast");
                            Log.d("JSON_ARRAY", jsonArray.toString());

                            for (int i = 0; i < jsonArray.length(); i++) {
                                cast.add(new Cast(jsonArray.getJSONObject(i).getString(JsonTAG.NAME),
                                        jsonArray.getJSONObject(i).getString(JsonTAG.CHARACTER),
                                        jsonArray.getJSONObject(i).getString(JsonTAG.IMAGE_PATH)));
                            }

                            castAdapter = new CastRecyclerViewAdapter(getActivity(), cast);
                            castRecyclerView.setAdapter(castAdapter);
                            fillTextViews();
                            Log.d("CASTS", "" + cast.size());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("JSON_ERROR", error.toString());
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    private void getVideoUrl() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constants.URL_VIDEO + movie.getId() + Constants.VIDEO_KEY,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = response.getJSONArray(JsonTAG.RESULTS);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            movie.setVideoUrl("https://www.youtube.com/watch?v=" + jsonObject.getString(JsonTAG.KEY));
                            Log.d("VIDEO", movie.getVideoUrl());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("JSON_ERROR", error.toString());
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    private void fillTextViews() {
        title.setText(movie.getTitle());
        rating.setText("R");
        releaseDate.setText(movie.getReleaseDate());
        runTime.setText(movie.getRunTime());
        revenue.setText(movie.getRevenue());
        avrgVote.setText(movie.getVoteAverage() + "");
        overview.setText(movie.getOverview());
        storyline.setText(movie.getStoryline());
        votes.setText("(" + movie.getVoteCount() + " votes)");
        ratingBar.setRating((float) movie.getVoteAverage());
        fragmentListener.setToolbarTitle(movie.getTitle());
    }

    private void setUpTextViews(View view) {
        title = (TextView) view.findViewById(R.id.textview_title);
        rating = (TextView) view.findViewById(R.id.textview_rating);
        releaseDate = (TextView) view.findViewById(R.id.textivew_date);
        runTime = (TextView) view.findViewById(R.id.textview_run_time);
        revenue = (TextView) view.findViewById(R.id.textview_revenu);
        votes = (TextView) view.findViewById(R.id.textivew_votes);
        avrgVote = (TextView) view.findViewById(R.id.textview_avrg_vote);
        overview = (TextView) view.findViewById(R.id.textview_overview);
        storyline = (TextView) view.findViewById(R.id.textivew_storyline);
    }


    public interface FragmentListener{
        void setToolbarTitle(String title);
    }

}
