package si.pernat.primoz.androidReveel.volley;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Primoz on 23.1.16.
 */
public class VolleySingleton {
    private static VolleySingleton volleySingleton = null;
    private RequestQueue mRequestQueue;

    private static Context mContext;

    private VolleySingleton(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(context);

    }

    public static synchronized VolleySingleton getInstance(Context context) {
        if (volleySingleton == null)
            volleySingleton = new VolleySingleton(context);
        return volleySingleton;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }
}